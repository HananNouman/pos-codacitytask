const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const db = require('./db');


const app = express();
 
//variable to know the record id;
var recordid=0;
const classes=[1,5,10,20,50];

// function to calculate transaction stages
var transactions=function(money){
   var count={50:0,20:0,10:0,5:0,1:0};
   var sum=0;
   function main(n)
     {
      if (sum==money )
         return 0;
      if (classes[n] > money-sum)
         return main(n-1);
      else {
         count[classes[n]]++;
         sum=sum+classes[n];
         if(classes[n]>money-sum){
          return main(n-1);
         } 
         else{
          return main(n);
         }
       }
     }
    main(4);
    return count;
}



app.use(express.static(path.join(__dirname ,'dist')));

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());


//handling requests from client and dealing with data base tables



//check the email and send the cashier id.
app.post('/email', function (req, res) {
  var queryStr = mysql.format('select id from emails where email= ?',[req.body.value]);
  db.query(queryStr, function(err, results) {
    res.json(results[0].id);
   });
})


//save records to show them when required and save transactions.
app.post('/saverecord', function (req, res) {
  recordid++;
  var queryStr ='INSERT INTO records (emailsid,Cost,Paid,amountofchange) VAlUES (?,?,?,?)';
  db.query(queryStr,[req.body.id,req.body.price,req.body.deposit,req.body.change],function(err,results){
       var deposittrans=transactions(req.body.deposit);
       var querydep ='INSERT INTO transactions (recordsid,types,bill,amount) VAlUES (?,?,?,?)';
       for (var i=0;i<5;i++){
          if(deposittrans[classes[i]]>0){
            db.query(querydep,[recordid,"deposit",classes[i],deposittrans[classes[i]]],function(err,results){
              
              });
          }
        }
        var withdrawtrans=transactions(req.body.change);
        var querywith ='INSERT INTO transactions (recordsid,types,bill,amount) VAlUES (?,?,?,?)';
        for (var i=0;i<5;i++){
          if(withdrawtrans[classes[i]]>0){
            db.query(querywith,[recordid,"withdraw",classes[i],withdrawtrans[classes[i]]],function(err,results){
              
              });
          }
        }
        res.json();
   });
   
});


app.post('/getrecords', function (req, res) {
  console.log(req.body.name.split(" ")[0])
  var queryStr0 = mysql.format('SELECT id from emails where lastname= ?',[req.body.name.split(" ")[1]]);

    db.query(queryStr0,function(err,results){
     console.log(results)
    var queryStr = mysql.format('SELECT * FROM records WHERE emailsid =?',[results[0].id]);
    db.query(queryStr,function(err,resultss){
    console.log(Array.isArray(resultss));
    res.json(resultss);
  })
    
  })

})



const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Our app is running on port ${ PORT }`);
});