import { Component } from '@angular/core';
import {logInService} from './log-in.service';
import {Observable} from 'rxjs/Rx';

@Component ({
  selector: 'data',
  template: `
  <br> 
  <br>
  <div style="text-align:center">
  <input placeholder="price" [(ngModel)] ="price"/>
  <input placeholder="deposit" [(ngModel)] ="deposit"/>
  <button (click)="cal()">calculate change</button>
  <h1>{{change}}</h1>
  <button (click)="save()">saveeee</button>
  <button><a [routerLink] = "['/']">BACK</a></button>
  </div>
  `
})
export class Appdata  {
	price: any;
  deposit: any;
  change: any;
  constructor(private login: logInService) { 
  }

  cal() {
     this.change=this.deposit-this.price;
   }

   save() {
   this.login.savetrans({"price":this.price,"deposit":this.deposit,"change":this.change}).subscribe(data => {
    },(err)=>{console.log("errooor")});
   }
}

