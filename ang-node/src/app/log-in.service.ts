import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class logInService {
    iddd: any;
    constructor(private http:HttpClient) {}

    getId(email) {
        return  this.http.post('/email',email);
    }

     savetrans(record) {
       return this.http.post('/saverecord',{"id":this.iddd,"price":record.price,"deposit":record.deposit,"change":record.change});
    }

    sendtrans(name) {
       return this.http.post('/getrecords',name).map((data) =>{return data});
    }
}