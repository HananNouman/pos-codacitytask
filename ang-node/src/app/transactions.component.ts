import { Component } from '@angular/core';
import {logInService} from './log-in.service';
import {Observable} from 'rxjs/Rx';
  

//to get cashier transactions
@Component ({
  selector: 'transactions',
  template: `
  <br> 
  <br>
  <div style="text-align:center">
  <select [(ngModel)]="name" >
  <option value="John Nouman">John Nouman</option>
  <option value="Peter Saies">Peter Saies</option>
  <option value="Amy AlAbsi">Amy AlAbsi</option>
  <option value="Hannah Hakous">Hannah Hakous</option>
  <option value="Michael Abozemaa">Michael Abozemaa</option>
  </select>
  <button (click)="show(name)">show transactions</button><button>
  <a [routerLink] = "['/']">BACK</a>
  </button>
  <div *ngIf="records">
  <li *ngFor="let item of records">
  <br>
  <span>{{item.DT}} </span>
  <span>cost {{item.Cost}} </span><span>paid {{item.Paid}} </span>
  <span>change {{item.amountofchange}} </span>
  </li>
  </div>
  </div>
  `
})

export   class   Apptransactions  {
	  name:any;
    records:any;
    counter:any;
    
	constructor(private login: logInService) { 
    }

   show(namee) {

    if(this.name){
      
      this.login.sendtrans({"name":this.name}).subscribe(data => {
      //alert(data)
      this.records=data;
    },(err)=>{console.log("errooor")});
   }
    }
}