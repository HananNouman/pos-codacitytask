import { Component } from '@angular/core';
import {logInService} from './log-in.service';
import {Appdata} from './data.component';
import {Observable} from 'rxjs/Rx';

@Component ({
  selector: 'logIn',
  template: `
  <br> 
  <br>
  <div style="text-align:center">
  <input placeholder="enter your email" [(ngModel)] ="value" *ngIf="!id" style="padding:20px 30px"/>
  <button (click)="get()" *ngIf="!id" style="padding:20px 30px">login</button>
  <data *ngIf="id"></data>
   </div>
   `
})
export   class   ApplogIn  {
  value: any;
  id: any;
  constructor(private login: logInService) { 
  }

   get() {
     this.login.getId({"value":this.value}).subscribe(data => {
     this.login.iddd=data;
     //alert(this.login.iddd);
     this.id=data;
     //alert(this.id);
    },(err)=>{console.log("errooor")});
   }
}
