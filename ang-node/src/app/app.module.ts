import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent }  from './app.component';
import { Approuts } from './routs.component';
import { ApplogIn } from './logIn.component';
import { Appdata } from './data.component';
import { logInService } from './log-in.service';
import { Apptransactions } from './transactions.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {path: '', component: Approuts},
  { path: 'logIn', component: ApplogIn },
  { path: 'transactions', component: Apptransactions }
];

@NgModule ({
  imports: [ BrowserModule,
  RouterModule.forRoot(appRoutes),FormsModule,HttpClientModule ],
  declarations: [ AppComponent,Approuts,ApplogIn,Apptransactions,Appdata],
  providers: [logInService],
  bootstrap: [ AppComponent ]
})
export class AppModule { }