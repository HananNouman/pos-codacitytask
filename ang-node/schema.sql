CREATE DATABASE POS;

USE POS;

CREATE TABLE emails (
  id int NOT NULL AUTO_INCREMENT,
  firstname varchar(20),
  lastname varchar(20),
  email varchar(50),
  PRIMARY KEY (ID)
);


CREATE TABLE records (
  id        int    NOT NULL AUTO_INCREMENT,
  DT DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  emailsid  int,
  Cost      int    ,
  Paid      int    ,
  amountofchange    int  ,  
  PRIMARY KEY (ID)
);

CREATE TABLE transactions (
  id        int    NOT NULL AUTO_INCREMENT,
  recordsid int,
  types    varchar(50),
  bill    int,
  amount    int,
 PRIMARY KEY (ID)
);