var mysql = require('mysql');


var connection = mysql.createConnection({
  user: 'root',
  password: '',
  database: 'POS'
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "INSERT INTO emails (firstname, lastname, email) VALUES ?";
  var values = [
    ['John', 'Nouman','hanannouman1993@gmail.com'],
    ['Peter', 'Saies','noor@hotmail.com'],
    ['Amy', 'AlAbsi','bushar@hotmil.com'],
    ['Hannah', 'Hakous','asmaa@gmail.com'],
    ['Michael', 'Abozemaa','abrar@gmail.com']
  ];
  connection.query(sql, [values], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });
});

module.exports = connection;